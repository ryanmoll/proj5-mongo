# Copied from flask_brevets.py
import flask
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

# Original app.py 
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

# Globals
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


# New routes -IN DEVELOPMENT-
@app.route('/submit', methods=['POST'])
def submit():
    data = request.form                             # Create variable 'data' and populate it with the raw form data

    miles = []                                      # Initialize list miles to hold all miles values
    km = []                                         # Initialize list km to hold all km values
    location = []                                   # Initialize list location to hold all location values
    open_times = []                                 # Initialize list open_times to hold all open time values
    close_times = []                                # Initialize list close_times to hold all close time values
    size = 0                                        # Initialize size for number of checkpoints in request

    for key in data.keys():                         # For each key that appears in the form data
        for value in data.getlist(key):             # For each value of each key
            if key == 'miles' and value != '':      # If the key is miles and it has a value
                miles.append(value)                 # Add that value to the miles list
                size += 1                           # Update size as long as there are still values
            elif key == 'km' and value != '':       # Repeat for all other keys...
                km.append(value)
            elif key == 'open' and value != '':
                open_times.append(value)
            elif key == 'close' and value != '':
                close_times.append(value)
            elif key == 'distance':
                dist = value                        # Create a variable with distance value

    i = 0
    for value in data.getlist('location'):          # For every value with the location key
        location.append(value)                      # Add the location value to its list
        i += 1
        if i == size:                               # Don't want to add a bunch of null locations to that list
            break

    db.tododb.remove({})                            # Clear all the stuff in the database before inserting new stuff
    for x in range(size):
        item_doc = {                                # Create a single item to be added to the database
           'distance': dist,                        # Add distance to the database
           'miles': miles[x],                       # Add each of the items extracted from the form data in key-value pairs
           'km': km[x],
           'location': location[x],
           'open_time': open_times[x],
           'close_time': close_times[x]
        }
        db.tododb.insert_one(item_doc)              # Add current item to the  database
        dist = ""                                   # Give dist a null value so it's only passed in once
    return render_template('calc.html')  # "Keep going"


# Routes from app.py
@app.route('/done')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)

# Routes from flask_brevets.py
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', -1, type=float)                         # Retrieve the km val passed in. Default to -1
    begin_time = request.args.get('begin_time')                         # Retrieve the begin_time val passed in
    begin_date = request.args.get('begin_date')                         # Retrieve the begin_date val passed in
    distance = request.args.get('distance')                             # Retrieve the distance val passed in
    app.logger.debug("request.args: {}".format(request.args))

    if km == -1:
        result = {"error": 2}
        return flask.jsonify(result=result)

    distance = float(distance)                                          # Convert distance form string to float so we can perform operations on it
    if km > distance * 1.2:                                             # More than 20% greater than the total brevet length is our cutoff
        result = {"error": 1}
        return flask.jsonify(result=result)                             # Abort now with error

    time = begin_date + " " + begin_time
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)

    result = {"open": open_time, "close": close_time, "error": 0}
    return flask.jsonify(result=result)


#############
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
