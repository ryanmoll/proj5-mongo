# Project 5: Brevet time calculator with Ajax and MongoDB

_____________________________________

Contact:

- Name: Ryan Moll

- Email: rmoll@uoregon.edu

_____________________________________

Info:

- This code is based on a skeleton defined by the project 4 code. That code can be found here: https://bitbucket.org/ryanmoll/proj4-brevets/

- This code differs from last time primarily in 2 files. The docker-compose.yml file creates the project as well as another container which runs a mongo database. The app.py file replaced the flask-brevets.py and added the functionality of adding data to a mongo database and retrieving it. 

- All of this code can be packaged up using Docker. Should you want to run this code with docker, use the commands 'docker-compose build' and 'docker-compose up' in succession.

_____________________________________

Possible Errors:

- The html page suppresses the submit button from being clicked until there is data input and that data is not too large. 

- The display button can be clicked and in the case that no data is passed in, it will take them to the standard result page with no data visible. There is an error message on the page that lets the user know what happened.

- Both the buttons have tooltip text that explains when they can be clicked or why they are not clickable that updates depending on the status. If the user is confused, they can hover over the buttons and see explanitory text.  